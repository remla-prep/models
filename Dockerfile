FROM python:3.7.10-slim AS prep

WORKDIR /root/
COPY requirements.txt .

RUN mkdir output &&\
	python -m pip install --upgrade pip &&\
	pip install -r requirements.txt

COPY smsspamcollection/ smsspamcollection/
COPY src/ src/

RUN	python src/read_data.py &&\
	python src/text_preprocessing.py &&\
	python src/text_classification.py


FROM python:3.7.10-slim

WORKDIR /root/
COPY --from=prep /root/output/ output/
COPY src/ src/

EXPOSE 8080

RUN echo "alias ls=\"ls -hal --color\"\n" >> .bashrc &&\
	python -m pip install --upgrade pip &&\
	pip install joblib flask flasgger pandas nltk sklearn &&\
	rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["python"]
CMD ["src/serve_model.py"]
